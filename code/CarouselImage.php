<?php
class CarouselImage extends DataObject {

    private static $db = array(
        'SortOrder'        => 'Int',
        'ImageTitle'       => 'Varchar(255)',
        'ImageDescription' => 'HTMLText',
        'ExternalLink'     => 'Varchar(255)',
        'PositionLeftRight' => "Enum('left,right','left')",
        'PositionVertical' => 'Int',
        'PositionHorizontal' => 'Int'
    );

    private static $has_one = array(
        'Carousel'       => 'Carousel',
        'Picture'          => 'Image',
        'InternalLink'     => 'SiteTree'
    );

    // columns in Grid
    static $summary_fields = array(
        'Picture.CMSThumbnail',
        'ImageTitle'
    );

    private static $translatable_fields = array(
        'ImageTitle',
        'ImageDescription'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['Picture.CMSThumbnail'] = _t('Carousel.IMAGAE', "Image");
        $labels['ImageTitle'] = _t('CarouselImage.IMAGETITLE', "Title");
        $labels['ImageDescription'] = _t('CarouselImage.IMAGEDESCRIPTION', "Description");
        $labels['Image'] = _t('CarouselImage.IMAGE', "Image");
        $labels['ExternalLink'] = _t('CarouselImage.EXTERNALLINK', "External link");
        $labels['InternalLink'] = _t('CarouselImage.INTERNALLINK', "Internal link");
        $labels['PositionLeftRight'] = _t('CarouselImage.POSITIONLEFTRIGHT', "Text position horizontal from");
        $labels['PositionVertical'] = _t('CarouselImage.POSITIONVERTICAL', "Text position vertical");
        $labels['PositionHorizontal'] = _t('CarouselImage.POSITIONHORIZONTAL', "Text position horizontal");
        $labels['PositionLeftRight_extra'] = _t('CarouselImage.POSITIONLEFTRIGHT_EXTRA', "Position horizontal from left or right");
        $labels['PositionVertical_extra'] = _t('CarouselImage.POSITIONVERTICAL_EXTRA', "Position from top in px");
        $labels['PositionHorizontal_extra'] = _t('CarouselImage.POSITIONHORIZONTAL_EXTRA', "Position from left or right in px");

        return $labels;
    }

    private static $default_sort = 'SortOrder';

    public function getCMSFields() {

        // Field labels
        $l = $this->fieldLabels();

        $thumbField = new UploadField('Picture', $l['Image']);
        $thumbField->allowedExtensions = array('jpg', 'png', 'gif');
        $imagetitlefield = TextField::create('ImageTitle', $l['ImageTitle']);
        $imagedescriptionfield = TextareaField::create('ImageDescription', $l['ImageDescription']);
        $externallinkfield = TextField::create('ExternalLink', $l['ExternalLink']);
        $internallinkfield = TreeDropdownField::create('InternalLinkID', $l['InternalLink'], 'SiteTree');
        $positionleftrightfield = DropdownField::create(
            'PositionLeftRight',
            $l['PositionLeftRight'],
            singleton('CarouselImage')->dbObject('PositionLeftRight')->enumValues()
        );
        $positionleftrightfield->setDescription($l['PositionLeftRight_extra']);
        $positionverticalfield = NumericField::create('PositionVertical', $l['PositionVertical']);
        $positionverticalfield->setDescription($l['PositionVertical_extra']);
        $positionhorizontalfield = NumericField::create('PositionHorizontal', $l['PositionHorizontal']);
        $positionhorizontalfield->setDescription($l['PositionHorizontal_extra']);

        if (class_exists('TranslatableDataObject')) {

            $fields = new FieldList();

            $fields->add($this->getTranslatableTabSet());
            // add all "Global" fields to another tab
            $fields->addFieldsToTab('Root.Global', array(
                $thumbField,
                $externallinkfield,
                $internallinkfield,
                $positionleftrightfield,
                $positionhorizontalfield,
                $positionverticalfield
            ));

        }else{

            $fields = parent::getCMSFields();

            $fields->removeByName(array(
                'Main'
            ));

            $fields->addFieldsToTab('Root.Main', array(
                $thumbField,
                $imagetitlefield,
                $imagedescriptionfield,
                $externallinkfield,
                $internallinkfield,
                $positionleftrightfield,
                $positionverticalfield,
                $positionhorizontalfield
            ));

        }

        return $fields;

    }

    public function canView($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }
    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }
    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }
    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}