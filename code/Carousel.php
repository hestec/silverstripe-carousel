<?php

class Carousel extends DataObject {

    private static $db = array(
        'Name' => 'Varchar(50)',
        'Description' => 'Text'
    );

    private static $has_many = array(
        'Images' => 'CarouselImage'
    );

    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

        $labels['Name'] = _t('Carousel.NAME', "Name");
        $labels['Description'] = _t('Carousel.DESCRIPTION', "Description");
        $labels['Description_optional'] = _t('Carousel.DESCRIPTION_OPTIONAL', "Description is optional.");

        return $labels;
    }

    public function getCMSFields() {

        $fields = parent::getCMSFields();

        // Field labels
        $l = $this->fieldLabels();

        $fields->removeByName(array(
            'Images'
        ));

        if ($this->ID == 0) {

            $ImagesGridField = LiteralField::create('ImagesDisclaimer', _t('Carousel.IMAGESDISCLAIMER','The images can be added once you have saved this carousel for the first time.'));

        } else {

            $BulkmanagerComponent = new GridFieldBulkManager();
            $BulkmanagerComponent->removeBulkAction('unLink');

            $ImagesGridField = new GridField(
                'Images',
                _t('Carousel.IMAGES', 'Images'),
                $this->Images(),
                GridFieldConfig::create()
                    ->addComponent(new GridFieldToolbarHeader())
                    ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
                    ->addComponent(new GridFieldSortableHeader())
                    ->addComponent(new GridFieldDataColumns())
                    //->addComponent(new GridFieldPaginator(50))
                    //->addComponent(new GridFieldEditableColumns())
                    ->addComponent(new GridFieldEditButton())
                    ->addComponent(new GridFieldDeleteAction())
                    ->addComponent(new GridFieldDetailForm())
                    ->addComponent(new GridFieldFilterHeader())
                    ->addComponent(new GridFieldOrderableRows('SortOrder'))
                    ->addComponent(new GridFieldBulkUpload())
                    ->addComponent($BulkmanagerComponent)
            );
        }

        $namefield = TextField::create('Name', $l['Name']);
        $descriptionfield = TextareaField::create('Description', $l['Description']);
        $descriptionfield->setDescription($l['Description_optional']);

        $fields->addFieldsToTab('Root.Main', array(
            $namefield,
            $descriptionfield,
            $ImagesGridField
        ));

        /*
        $fields=parent::getCMSFields();

        $conf=GridFieldConfig_RecordEditor::create(10);
        $conf->addComponent(new GridFieldOrderableRows('SortOrder'));

        $fields->addFieldToTab('Root.Images', new GridField('Images', 'Images', $this->Images(), $conf));
*/
        return $fields;

    }

    public function canView($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }
    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }
    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }
    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

}
