<?php
class CarouselAdmin extends ModelAdmin {

    private static $managed_models = array(
        'Carousel'
    );

    // disable the importer
    private static $model_importers = array();

    // Linked as /admin/slides/
    static $url_segment = 'carousel';

    // title in cms navigation
    static $menu_title = 'Carousel';

    // menu icon
    static $menu_icon = 'carousel/images/icons/carousel_icon.png';


}
